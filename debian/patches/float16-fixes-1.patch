Origin: backport, https://github.com/marzer/tomlplusplus/commit/c8780a5b8e8d2f8ff1fd747a3a89b7becebfdee9
Last-Update: 2022-12-20

From c8780a5b8e8d2f8ff1fd747a3a89b7becebfdee9 Mon Sep 17 00:00:00 2001
From: Mark Gillard <marzer_@hotmail.com>
Date: Mon, 17 Oct 2022 20:08:26 +0300
Subject: [PATCH] fixed some `_Float16` detection issues

also:
- fixed a few minor issues with tests
- removed `__fp16` support (it was always broken)
---
 .github/workflows/ci.yaml                  |   2 +-
 .github/workflows/gh-pages.yaml            |   2 +-
 CHANGELOG.md                               |   2 +
 docs/poxy.toml                             |   2 +
 include/toml++/impl/forward_declarations.h |  10 --
 include/toml++/impl/preprocessor.h         | 113 ++++++++++++++++-----
 include/toml++/toml.h                      |   8 +-
 meson.build                                |  30 +-----
 tests/conformance_burntsushi_invalid.cpp   |  10 +-
 tests/impl_toml.cpp                        |   6 --
 tests/meson.build                          |  13 +--
 tests/tests.h                              |  20 +++-
 toml.hpp                                   |  93 ++++++++++-------
 vendor/catch.hpp                           |  28 +++--
 14 files changed, 196 insertions(+), 143 deletions(-)

diff --git a/include/toml++/impl/forward_declarations.h b/include/toml++/impl/forward_declarations.h
index 52559128..8922dce5 100644
--- a/include/toml++/impl/forward_declarations.h
+++ b/include/toml++/impl/forward_declarations.h
@@ -625,11 +625,6 @@ TOML_IMPL_NAMESPACE_START
 	template <typename T>
 	struct float_traits : float_traits_base<T, std::numeric_limits<T>::digits, std::numeric_limits<T>::digits10>
 	{};
-#ifdef TOML_FP16
-	template <>
-	struct float_traits<TOML_FP16> : float_traits_base<TOML_FP16, __FLT16_MANT_DIG__, __FLT16_DIG__>
-	{};
-#endif
 #ifdef TOML_FLOAT16
 	template <>
 	struct float_traits<TOML_FLOAT16> : float_traits_base<TOML_FLOAT16, __FLT16_MANT_DIG__, __FLT16_DIG__>
@@ -651,11 +646,6 @@ TOML_IMPL_NAMESPACE_START
 	template <>
 	struct value_traits<long double> : float_traits<long double>
 	{};
-#ifdef TOML_FP16
-	template <>
-	struct value_traits<TOML_FP16> : float_traits<TOML_FP16>
-	{};
-#endif
 #ifdef TOML_FLOAT16
 	template <>
 	struct value_traits<TOML_FLOAT16> : float_traits<TOML_FLOAT16>
diff --git a/include/toml++/impl/preprocessor.h b/include/toml++/impl/preprocessor.h
index 44c990a3..63c189bd 100644
--- a/include/toml++/impl/preprocessor.h
+++ b/include/toml++/impl/preprocessor.h
@@ -31,7 +31,7 @@
 #endif
 
 //#=====================================================================================================================
-//# COMPILER/OS/ARCH DETECTION
+//# COMPILER / OS
 //#=====================================================================================================================
 
 #ifdef __clang__
@@ -75,11 +75,46 @@
 #else
 #define TOML_INTELLISENSE 0
 #endif
+
+//#=====================================================================================================================
+//# ARCHITECTURE
+//#=====================================================================================================================
+
+// IA64
+#if defined(__ia64__) || defined(__ia64) || defined(_IA64) || defined(__IA64__) || defined(_M_IA64)
+#define TOML_ARCH_ITANIUM 1
+#else
+#define TOML_ARCH_ITANIUM 0
+#endif
+
+// AMD64
+#if defined(__amd64__) || defined(__amd64) || defined(__x86_64__) || defined(__x86_64) || defined(_M_AMD64)
+#define TOML_ARCH_AMD64 1
+#else
+#define TOML_ARCH_AMD64 0
+#endif
+
+// 32-bit x86
+#if defined(__i386__) || defined(_M_IX86)
+#define TOML_ARCH_X86 1
+#else
+#define TOML_ARCH_X86 0
+#endif
+
+// ARM
 #if defined(__aarch64__) || defined(__ARM_ARCH_ISA_A64) || defined(_M_ARM64) || defined(__ARM_64BIT_STATE)             \
-	|| defined(__arm__) || defined(_M_ARM) || defined(__ARM_32BIT_STATE)
-#define TOML_ARM 1
+	|| defined(_M_ARM64EC)
+#define TOML_ARCH_ARM32 0
+#define TOML_ARCH_ARM64 1
+#define TOML_ARCH_ARM	1
+#elif defined(__arm__) || defined(_M_ARM) || defined(__ARM_32BIT_STATE)
+#define TOML_ARCH_ARM32 1
+#define TOML_ARCH_ARM64 0
+#define TOML_ARCH_ARM	1
 #else
-#define TOML_ARM 0
+#define TOML_ARCH_ARM32 0
+#define TOML_ARCH_ARM64 0
+#define TOML_ARCH_ARM	0
 #endif
 
 //#=====================================================================================================================
@@ -1016,40 +1051,62 @@ TOML_ENABLE_WARNINGS;
 //# }}
 
 //#=====================================================================================================================
-//# EXTENDED INT AND FLOAT TYPES
+//# FLOAT16
 //#=====================================================================================================================
-// clang-format off
 
-#ifdef __FLT16_MANT_DIG__
-	#if __FLT_RADIX__ == 2					\
-			&& __FLT16_MANT_DIG__ == 11		\
-			&& __FLT16_DIG__ == 3			\
-			&& __FLT16_MIN_EXP__ == -13		\
-			&& __FLT16_MIN_10_EXP__ == -4	\
-			&& __FLT16_MAX_EXP__ == 16		\
-			&& __FLT16_MAX_10_EXP__ == 4
-		#if TOML_ARM && (TOML_GCC || TOML_CLANG)
-			#define TOML_FP16 __fp16
-		#endif
-		#if TOML_ARM && TOML_CLANG // not present in g++
-			#define TOML_FLOAT16 _Float16
-		#endif
-	#endif
+#if TOML_CLANG
+//# {{
+//	Excerpt from https://clang.llvm.org/docs/LanguageExtensions.html:
+//
+//	_Float16 is currently only supported on the following targets,
+//	with further targets pending ABI standardization:
+//
+//		32-bit ARM
+//		64-bit ARM (AArch64)
+//		AMDGPU
+//		SPIR
+//		X86 as long as SSE2 is available
+//
+//# }}
+#if (TOML_ARCH_ARM || TOML_ARCH_X86 || TOML_ARCH_AMD64) && defined(__FLT16_MANT_DIG__)
+#define TOML_FLOAT16 _Float16
+#endif
+#elif TOML_GCC
+//# {{
+//	Excerpt from https://gcc.gnu.org/onlinedocs/gcc/Floating-Types.html:
+//
+//	The _Float16 type is supported on AArch64 systems by default, on ARM systems when the IEEE format for
+//	16-bit floating-point types is selected with -mfp16-format=ieee and,
+//	for both C and C++, on x86 systems with SSE2 enabled.
+//
+//	*** except: the bit about x86 seems incorrect?? ***
+//# }}
+/*
+
+ */
+#if (TOML_ARCH_ARM /*|| TOML_ARCH_X86 || TOML_ARCH_AMD64*/) && defined(__FLT16_MANT_DIG__)
+#define TOML_FLOAT16 _Float16
+#endif
 #endif
 
-#if defined(__SIZEOF_FLOAT128__)		\
-	&& defined(__FLT128_MANT_DIG__)		\
-	&& defined(__LDBL_MANT_DIG__)		\
+//#=====================================================================================================================
+//# FLOAT128
+//#=====================================================================================================================
+
+#if defined(__SIZEOF_FLOAT128__) && defined(__FLT128_MANT_DIG__) && defined(__LDBL_MANT_DIG__)                         \
 	&& __FLT128_MANT_DIG__ > __LDBL_MANT_DIG__
-	#define TOML_FLOAT128	__float128
+#define TOML_FLOAT128 __float128
 #endif
 
+//#=====================================================================================================================
+//# INT128
+//#=====================================================================================================================
+
 #ifdef __SIZEOF_INT128__
-	#define TOML_INT128		__int128_t
-	#define TOML_UINT128	__uint128_t
+#define TOML_INT128	 __int128_t
+#define TOML_UINT128 __uint128_t
 #endif
 
-// clang-format on
 //#====================================================================================================================
 //# VERSIONS AND NAMESPACES
 //#====================================================================================================================
diff --git a/include/toml++/toml.h b/include/toml++/toml.h
index 30ca7e73..f7db954c 100644
--- a/include/toml++/toml.h
+++ b/include/toml++/toml.h
@@ -90,7 +90,12 @@ TOML_POP_WARNINGS;
 #undef TOML_ANON_NAMESPACE
 #undef TOML_ANON_NAMESPACE_END
 #undef TOML_ANON_NAMESPACE_START
-#undef TOML_ARM
+#undef TOML_ARCH_ARM
+#undef TOML_ARCH_AMD64
+#undef TOML_ARCH_ARM32
+#undef TOML_ARCH_ARM64
+#undef TOML_ARCH_ITANIUM
+#undef TOML_ARCH_X86
 #undef TOML_ASSERT
 #undef TOML_ASSERT_ASSUME
 #undef TOML_ASSUME
@@ -127,7 +132,6 @@ TOML_POP_WARNINGS;
 #undef TOML_FLOAT_CHARCONV
 #undef TOML_FLOAT128
 #undef TOML_FLOAT16
-#undef TOML_FP16
 #undef TOML_GCC
 #undef TOML_HAS_ATTR
 #undef TOML_HAS_BUILTIN
diff --git a/meson.build b/meson.build
index 4ec3c94d..c8152253 100644
--- a/meson.build
+++ b/meson.build
@@ -250,9 +250,6 @@ compiler_supports_char8_args = []
 if is_gcc or is_clang
 	compiler_supports_char8_args += '-fchar8_t'
 endif
-compiler_supports_char8_args_private = []
-compiler_supports_char8_args_private += compiler_supports_cpp20_args
-compiler_supports_char8_args_private += compiler_supports_char8_args
 compiler_supports_char8 = compiler_supports_cpp20 and compiler.links('''
 	#include <version>
 	#include <string_view>
@@ -278,7 +275,7 @@ compiler_supports_char8 = compiler_supports_cpp20 and compiler.links('''
 	}
 	''',
 	name: 'supports char8_t',
-	args: compiler_supports_char8_args_private
+	args: [ compiler_supports_cpp20_args, compiler_supports_char8_args ]
 )
 
 #######################################################################################################################
@@ -325,27 +322,6 @@ compiler_supports_consteval_properly = compiler_supports_consteval and not compi
 # __fp16 and _Float16 checks
 #######################################################################################################################
 
-float_16_preprocessor_single_check_template = '''
-	#ifndef @0@
-		#error @0@ wasn't defined!
-	#else
-		#pragma message("@0@: " MAKE_STRING(@0@))
-	#endif
-	#if @0@ != @1@
-		#error @0@ was not @1@!
-	#endif
-'''
-float_16_preprocessor_checks = '''
-	#define MAKE_STRING(s)		MAKE_STRING_1(s)
-	#define MAKE_STRING_1(s)	#s
-	''' + float_16_preprocessor_single_check_template.format('__FLT_RADIX__', '2')						\
-	+ float_16_preprocessor_single_check_template.format('__FLT16_MANT_DIG__', '11')					\
-	+ float_16_preprocessor_single_check_template.format('__FLT16_DIG__', '3')							\
-	+ float_16_preprocessor_single_check_template.format('__FLT16_MIN_EXP__', '-13')					\
-	+ float_16_preprocessor_single_check_template.format('__FLT16_MIN_10_EXP__', '-4')					\
-	+ float_16_preprocessor_single_check_template.format('__FLT16_MAX_EXP__', '16')						\
-	+ float_16_preprocessor_single_check_template.format('__FLT16_MAX_10_EXP__', '4')
-
 compiler_supports_float16_args = []
 if is_gcc
 	compiler_supports_float16_args += '-mfp16-format=ieee'
@@ -364,8 +340,6 @@ compiler_supports_fp16 = compiler.links('''
 	args: compiler_supports_float16_args
 )
 compiler_supports_float16 = compiler.links('''
-	@0@
-
 	int main()
 	{
 		static_assert(sizeof(_Float16) == 2);
@@ -374,7 +348,7 @@ compiler_supports_float16 = compiler.links('''
 		const auto f3 = static_cast<_Float16>(0.2L);
 		return 0;
 	}
-	'''.format(float_16_preprocessor_checks),
+	''',
 	name: 'supports _Float16',
 	args: compiler_supports_float16_args
 )
diff --git a/tests/conformance_burntsushi_invalid.cpp b/tests/conformance_burntsushi_invalid.cpp
index e25086de..a4114140 100644
--- a/tests/conformance_burntsushi_invalid.cpp
+++ b/tests/conformance_burntsushi_invalid.cpp
@@ -430,16 +430,16 @@ zyx = 42)"sv;
 zyx = 42)"sv;
 	static constexpr auto table_quoted_no_close				= R"(["where will it end]
 name = value)"sv;
-	static constexpr auto table_redefine					= R"(# Define b as int, and try to use it as a table: error
+	static constexpr auto table_redefine		 = R"(# Define b as int, and try to use it as a table: error
 [a]
 b = 1
 
 [a.b]
 c = 2)"sv;
-	static constexpr auto table_rrbrace						= R"([[table] ])"sv;
-	static constexpr auto table_text_after_table			= R"([error] this shouldn't be here)"sv;
-	static constexpr auto table_whitespace					= R"([invalid key])"sv;
-	static constexpr auto table_with_pound					= R"([key#group]
+	static constexpr auto table_rrbrace			 = R"([[table] ])"sv;
+	static constexpr auto table_text_after_table = R"([error] this shouldn't be here)"sv;
+	static constexpr auto table_whitespace		 = R"([invalid key])"sv;
+	static constexpr auto table_with_pound		 = R"([key#group]
 answer = 42)"sv;
 }
 
diff --git a/tests/impl_toml.cpp b/tests/impl_toml.cpp
index 41c31515..37c02a66 100644
--- a/tests/impl_toml.cpp
+++ b/tests/impl_toml.cpp
@@ -64,9 +64,6 @@ namespace toml
 	CHECK_CAN_REPRESENT_NATIVE(TOML_INT128, true);
 	CHECK_CAN_REPRESENT_NATIVE(TOML_UINT128, false);
 #endif
-#ifdef TOML_FP16
-	CHECK_CAN_REPRESENT_NATIVE(TOML_FP16, false);
-#endif
 #ifdef TOML_FLOAT16
 	CHECK_CAN_REPRESENT_NATIVE(TOML_FLOAT16, false);
 #endif
@@ -292,9 +289,6 @@ namespace toml
 	CHECK_INSERTED_AS(uint32_t, value<int64_t>);
 	CHECK_INSERTED_AS(float, value<double>);
 	CHECK_INSERTED_AS(double, value<double>);
-#ifdef TOML_FP16
-	CHECK_INSERTED_AS(TOML_FP16, value<double>);
-#endif
 #ifdef TOML_FLOAT16
 	CHECK_INSERTED_AS(TOML_FLOAT16, value<double>);
 #endif
diff --git a/tests/meson.build b/tests/meson.build
index f4ab11e3..6c3b8112 100644
--- a/tests/meson.build
+++ b/tests/meson.build
@@ -40,10 +40,10 @@ endif
 #######################################################################################################################
 
 compiler_supports_fast_math_args = []
-if compiler.get_id() == 'gcc' or compiler.get_id() == 'clang'
+if is_gcc or is_clang
 	compiler_supports_fast_math_args += '-ffast-math'
 	compiler_supports_fast_math_args += '-ffp-contract=fast'
-elif compiler.get_id() == 'msvc' or compiler.get_id() == 'intel-cl'
+elif is_msvc or is_icc_cl
 	compiler_supports_fast_math_args += '/fp:fast'
 endif
 compiler_supports_fast_math = compiler.links('''
@@ -186,13 +186,8 @@ foreach cpp20 : cpp20_modes
 					test_args += compiler_supports_fast_math_args
 				endif
 
-				if compiler_supports_float16 or compiler_supports_fp16
-					if compiler_supports_fp16
-						test_args += '-DSHOULD_HAVE_FP16=1'
-					endif
-					if compiler_supports_float16
-						test_args += '-DSHOULD_HAVE_FLOAT16=1'
-					endif
+				if compiler_supports_float16
+					test_args += '-DSHOULD_HAVE_FLOAT16=1'
 				endif
 				if compiler_supports_int128
 					test_args += '-DSHOULD_HAVE_INT128=1'
diff --git a/tests/tests.h b/tests/tests.h
index b4f6a66e..5756a270 100644
--- a/tests/tests.h
+++ b/tests/tests.h
@@ -11,9 +11,6 @@
 #else
 #include "../include/toml++/toml.h"
 #endif
-#if defined(TOML_FP16) ^ SHOULD_HAVE_FP16
-#error TOML_FP16 was not deduced correctly
-#endif
 #if defined(TOML_FLOAT16) ^ SHOULD_HAVE_FLOAT16
 #error TOML_FLOAT16 was not deduced correctly
 #endif
@@ -410,3 +407,20 @@ namespace Catch
 		extern template std::string stringify(const node_view<const node>&);
 	}
 }
+
+#if TOML_CPP >= 20 && TOML_CLANG && TOML_CLANG <= 14 // https://github.com/llvm/llvm-project/issues/55560
+
+TOML_PUSH_WARNINGS;
+TOML_DISABLE_WARNINGS;
+
+namespace
+{
+	[[maybe_unused]] static std::u8string clang_string_workaround(const char8_t* a, const char8_t* b)
+	{
+		return { a, b };
+	}
+}
+
+TOML_POP_WARNINGS;
+
+#endif
diff --git a/toml.hpp b/toml.hpp
index c67e3f91..c71d32e9 100644
--- a/toml.hpp
+++ b/toml.hpp
@@ -111,11 +111,42 @@
 #else
 #define TOML_INTELLISENSE 0
 #endif
+
+// IA64
+#if defined(__ia64__) || defined(__ia64) || defined(_IA64) || defined(__IA64__) || defined(_M_IA64)
+#define TOML_ARCH_ITANIUM 1
+#else
+#define TOML_ARCH_ITANIUM 0
+#endif
+
+// AMD64
+#if defined(__amd64__) || defined(__amd64) || defined(__x86_64__) || defined(__x86_64) || defined(_M_AMD64)
+#define TOML_ARCH_AMD64 1
+#else
+#define TOML_ARCH_AMD64 0
+#endif
+
+// 32-bit x86
+#if defined(__i386__) || defined(_M_IX86)
+#define TOML_ARCH_X86 1
+#else
+#define TOML_ARCH_X86 0
+#endif
+
+// ARM
 #if defined(__aarch64__) || defined(__ARM_ARCH_ISA_A64) || defined(_M_ARM64) || defined(__ARM_64BIT_STATE)             \
-	|| defined(__arm__) || defined(_M_ARM) || defined(__ARM_32BIT_STATE)
-#define TOML_ARM 1
+	|| defined(_M_ARM64EC)
+#define TOML_ARCH_ARM32 0
+#define TOML_ARCH_ARM64 1
+#define TOML_ARCH_ARM	1
+#elif defined(__arm__) || defined(_M_ARM) || defined(__ARM_32BIT_STATE)
+#define TOML_ARCH_ARM32 1
+#define TOML_ARCH_ARM64 0
+#define TOML_ARCH_ARM	1
 #else
-#define TOML_ARM 0
+#define TOML_ARCH_ARM32 0
+#define TOML_ARCH_ARM64 0
+#define TOML_ARCH_ARM	0
 #endif
 
 // TOML_HAS_INCLUDE
@@ -898,39 +929,29 @@ TOML_ENABLE_WARNINGS;
 	TOML_REQUIRES(condition)
 #define TOML_HIDDEN_CONSTRAINT(condition, ...) TOML_CONSTRAINED_TEMPLATE(condition, __VA_ARGS__)
 
-// clang-format off
+#if TOML_CLANG
+#if (TOML_ARCH_ARM || TOML_ARCH_X86 || TOML_ARCH_AMD64) && defined(__FLT16_MANT_DIG__)
+#define TOML_FLOAT16 _Float16
+#endif
+#elif TOML_GCC
+/*
 
-#ifdef __FLT16_MANT_DIG__
-	#if __FLT_RADIX__ == 2					\
-			&& __FLT16_MANT_DIG__ == 11		\
-			&& __FLT16_DIG__ == 3			\
-			&& __FLT16_MIN_EXP__ == -13		\
-			&& __FLT16_MIN_10_EXP__ == -4	\
-			&& __FLT16_MAX_EXP__ == 16		\
-			&& __FLT16_MAX_10_EXP__ == 4
-		#if TOML_ARM && (TOML_GCC || TOML_CLANG)
-			#define TOML_FP16 __fp16
-		#endif
-		#if TOML_ARM && TOML_CLANG // not present in g++
-			#define TOML_FLOAT16 _Float16
-		#endif
-	#endif
+ */
+#if (TOML_ARCH_ARM /*|| TOML_ARCH_X86 || TOML_ARCH_AMD64*/) && defined(__FLT16_MANT_DIG__)
+#define TOML_FLOAT16 _Float16
+#endif
 #endif
 
-#if defined(__SIZEOF_FLOAT128__)		\
-	&& defined(__FLT128_MANT_DIG__)		\
-	&& defined(__LDBL_MANT_DIG__)		\
+#if defined(__SIZEOF_FLOAT128__) && defined(__FLT128_MANT_DIG__) && defined(__LDBL_MANT_DIG__)                         \
 	&& __FLT128_MANT_DIG__ > __LDBL_MANT_DIG__
-	#define TOML_FLOAT128	__float128
+#define TOML_FLOAT128 __float128
 #endif
 
 #ifdef __SIZEOF_INT128__
-	#define TOML_INT128		__int128_t
-	#define TOML_UINT128	__uint128_t
+#define TOML_INT128	 __int128_t
+#define TOML_UINT128 __uint128_t
 #endif
 
-// clang-format on
-
 // clang-format off
 
 //********  impl/version.h  ********************************************************************************************
@@ -1678,11 +1699,6 @@ TOML_IMPL_NAMESPACE_START
 	template <typename T>
 	struct float_traits : float_traits_base<T, std::numeric_limits<T>::digits, std::numeric_limits<T>::digits10>
 	{};
-#ifdef TOML_FP16
-	template <>
-	struct float_traits<TOML_FP16> : float_traits_base<TOML_FP16, __FLT16_MANT_DIG__, __FLT16_DIG__>
-	{};
-#endif
 #ifdef TOML_FLOAT16
 	template <>
 	struct float_traits<TOML_FLOAT16> : float_traits_base<TOML_FLOAT16, __FLT16_MANT_DIG__, __FLT16_DIG__>
@@ -1704,11 +1720,6 @@ TOML_IMPL_NAMESPACE_START
 	template <>
 	struct value_traits<long double> : float_traits<long double>
 	{};
-#ifdef TOML_FP16
-	template <>
-	struct value_traits<TOML_FP16> : float_traits<TOML_FP16>
-	{};
-#endif
 #ifdef TOML_FLOAT16
 	template <>
 	struct value_traits<TOML_FLOAT16> : float_traits<TOML_FLOAT16>
@@ -17056,7 +17067,12 @@ TOML_POP_WARNINGS;
 #undef TOML_ANON_NAMESPACE
 #undef TOML_ANON_NAMESPACE_END
 #undef TOML_ANON_NAMESPACE_START
-#undef TOML_ARM
+#undef TOML_ARCH_ARM
+#undef TOML_ARCH_AMD64
+#undef TOML_ARCH_ARM32
+#undef TOML_ARCH_ARM64
+#undef TOML_ARCH_ITANIUM
+#undef TOML_ARCH_X86
 #undef TOML_ASSERT
 #undef TOML_ASSERT_ASSUME
 #undef TOML_ASSUME
@@ -17093,7 +17109,6 @@ TOML_POP_WARNINGS;
 #undef TOML_FLOAT_CHARCONV
 #undef TOML_FLOAT128
 #undef TOML_FLOAT16
-#undef TOML_FP16
 #undef TOML_GCC
 #undef TOML_HAS_ATTR
 #undef TOML_HAS_BUILTIN
